const workTimeInput = document.getElementById("work-time");
const breakTimeInput = document.getElementById("break-time");
const timerDisplay = document.getElementById("timer");
const startButton = document.getElementById("start");
const pauseButton = document.getElementById("pause");
const resetButton = document.getElementById("reset");

let isWorkTime = true;
const toggleButton = document.getElementById("toggle");
let timerInterval = null;
let timeLeft = workTimeInput.value * 60;

function updateTimerDisplay() {
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft % 60;
  timerDisplay.textContent = `${minutes.toString().padStart(2, "0")}:${seconds
    .toString()
    .padStart(2, "0")}`;
}

function startTimer() {
  if (timerInterval) return;
  timerInterval = setInterval(() => {
    timeLeft--;
    if (timeLeft < 0) {
      isWorkTime = !isWorkTime;
      timeLeft = (isWorkTime ? workTimeInput : breakTimeInput).value * 60;
    }
    updateTimerDisplay();
  }, 1000);
  startConfetti();
}

function startConfetti() {
  const confettiContainer = document.getElementById("confetti");
  for (let i = 0; i < 100; i++) {
    const confetti = document.createElement("span");
    confetti.textContent = "🎉";
    confetti.style.position = "absolute";
    confetti.style.fontSize = "30px";
    confetti.style.animation = `confetti 3s ease-in-out ${
      Math.random() * 3
    }s infinite`;
    confetti.style.left = `${Math.random() * 100}vw`;
    confettiContainer.appendChild(confetti);
  }
}

function clearConfetti() {
  const confettiContainer = document.getElementById("confetti");
  while (confettiContainer.firstChild) {
    confettiContainer.firstChild.remove();
  }
}

function pauseTimer() {
  clearInterval(timerInterval);
  timerInterval = null;
  clearConfetti();
}

function resetTimer() {
  pauseTimer();
  isWorkTime = true;
  timeLeft = workTimeInput.value * 60;
  updateTimerDisplay();
  clearConfetti();
}

startButton.addEventListener("click", startTimer);
pauseButton.addEventListener("click", pauseTimer);
resetButton.addEventListener("click", resetTimer);

updateTimerDisplay();
